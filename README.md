# Mirror meetup
[Starting page](https://blue-cloud-mirror.mybluemix.net/home)\
[Code pattern](https://developer.ibm.com/patterns/cloud-showcase-blue-mirror/)\
[Git repository](https://github.com/IBM/blue-cloud-mirror)

[TensorFlow js examples](http://heidloff.net/article/tensorflowjs-visual-recognition)

[Face detection js](https://github.com/justadudewhohacks/face-api.js)\
[Demo](https://justadudewhohacks.github.io/face-api.js/webcam_face_tracking/)

[Face classification js](https://github.com/oarriaga/face_classification)\
[Demo](https://tupleblog.github.io/face-classification-js/webcam.html)

[Posenet js](https://github.com/tensorflow/tfjs-models/tree/master/posenet)\
[Demo](https://storage.googleapis.com/tfjs-models/demos/posenet/camera.html)

[Free IBM Cloud account](https://ibm.biz/BdzM9Y)